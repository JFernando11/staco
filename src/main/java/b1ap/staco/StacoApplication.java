package b1ap.staco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StacoApplication {

	public static void main(String[] args) {
		SpringApplication.run(StacoApplication.class, args);
	}

}
